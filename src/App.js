import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Home} from './components/Home'
import {Middle} from './components/Middle'
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hey this is my first react app
        </p>
        <Home />
        
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
